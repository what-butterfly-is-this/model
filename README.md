# What Butterfly Is This: Notebook

This notebook showcases dataset processing, training and evaluation of models and final performance testing.

[Open in Google Colab](https://colab.research.google.com/drive/1dQ5-YzSXGpnbc6-QXtULmt4dQcnBmsnZ)

## Development

to run this notebook locally install dependencies with pip

```
pip install -r requirements.txt
```

then start the jupyter notebook server with

```
jupyter notebook
```
